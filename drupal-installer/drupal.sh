#!/bin/bash
#Couleur
orange='\033[33m'
rose='\033[31m'
vert='\033[32m'
blanc='\033[37m'

echo "INSTALLATION DRUPAL"

#Déclaration de plusieurs variables
while [ -z ${name_dir} ]; do
    echo "${orange}Veuillez saisir le nom du répertoire où sera installé le Drupal (ex. : public_html) : ${blanc}"
    read name_dir
done

#Se placer dans le répertoire du Drupal
cd ../${name_dir}/

#Télécharger la dernière archive du Drupal
echo "${orange}Récupération de Drupal via Composer... ${blanc}"
php ../drupal-installer/composer2.phar create-project drupal/recommended-project:9.5.3 . --no-interaction
php ../drupal-installer/composer2.phar config minimum-stability dev
echo "${vert}Fin de récupération de Drupal via Composer... ${blanc}"

#Déclaration de plusieurs variables
echo "${orange}Informations du projet... ${blanc}"
while [ -z ${name_projet} ]; do
    echo "Veuillez saisir le nom temporaire du projet sans espace (ex. Drupal) : "
    read name_projet
done

while [ -z ${alias_projet} ]; do
    echo "Veuillez saisir l'alias du projet (ex. drupal) : "
    read alias_projet
done

while [ -z ${langue} ]; do
    echo "Veuillez saisir la langue par défaut du projet (fr si pas de multilingue, sinon en) : "
    read langue
done

echo "${orange}Base de données... ${blanc}"
while [ -z ${db_host} ]; do
    echo "Veuillez saisir l'hôte de la BDD (ex. localhost ou 172.16.5.26) : "
    read db_host
done

while [ -z ${db_user} ]; do
    echo "Veuillez saisir l'utilisateur de la BDD : "
    read db_user
done

while [ -z ${db_pass} ]; do
    echo "Veuillez saisir le mot de passe de la BDD : "
    read db_pass
done

while [ -z ${db_name} ]; do
    echo "Veuillez saisir le nom de la BDD : "
    read db_name
done

echo "${orange}Compte administrateur... ${blanc}"
while [ -z ${admin_mail} ]; do
    echo "Veuillez saisir l'email du super admin (admin_netcom_${alias_projet}) du site : "
    read admin_mail
done

while [ -z ${admin_pw} ]; do
    echo "Veuillez saisir le mot de passe du super admin (admin_netcom_${alias_projet}) du site : "
    read admin_pw
done

#Téléchargement des modules Drupal officiels obligatoires
echo "${orange}Installation de Drush... ${blanc}"
php ../drupal-installer/composer2.phar require drush/drush

#Installation du Drupal
echo "${orange}Installation de Drupal via vendor/drush/drush/drush... ${blanc}"
vendor/drush/drush/drush site:install standard --db-url=mysql://${db_user}:${db_pass}@${db_host}/${db_name} --site-name=${name_projet} --site-mail=${admin_mail} --account-name=admin_netcom_${alias_projet} --account-mail=${admin_mail} --account-pass=${admin_pw} --locale=${langue}
echo "${vert}Fin de l'installation de Drupal via vendor/drush/drush/drush... ${blanc}"

#Création du répertoire temporaire
echo "${orange}Création du répertoire temporaire... ${blanc}"
cd web
mkdir tmp
cd ../
mkdir private
chmod 755 private/

#BUG default.services.yml
cd web/sites/
chmod u+w default/
cd ../../

echo "${vert}Fin de la création du répertoire temporaire... ${blanc}"

#Téléchargement des modules Drupal officiels obligatoires
echo "${orange}Récupération des modules Drupal... ${blanc}"
#Modules d'amélioration du Drupal
php ../drupal-installer/composer2.phar require 'drupal/module_filter'
php ../drupal-installer/composer2.phar require 'drupal/admin_toolbar'
php ../drupal-installer/composer2.phar require 'drupal/views_bulk_operations'
php ../drupal-installer/composer2.phar require 'drupal/pathauto'
php ../drupal-installer/composer2.phar require 'drupal/ultimate_cron'
php ../drupal-installer/composer2.phar require 'drupal/role_delegation'
php ../drupal-installer/composer2.phar require 'drupal/simple_menu_permissions'
php ../drupal-installer/composer2.phar require 'drupal/allowed_formats'
php ../drupal-installer/composer2.phar require 'drupal/diff'
php ../drupal-installer/composer2.phar require 'drupal/redirect'
php ../drupal-installer/composer2.phar require 'drupal/scheduler'
php ../drupal-installer/composer2.phar require 'drupal/scheduler_content_moderation_integration'
php ../drupal-installer/composer2.phar require 'drupal/menu_item_extras'
php ../drupal-installer/composer2.phar require 'drupal/quick_node_clone'
php ../drupal-installer/composer2.phar require 'drupal/broken_link'

#Autre modules
php ../drupal-installer/composer2.phar require 'drupal/editor_advanced_link'
php ../drupal-installer/composer2.phar require 'drupal/menu_link_attributes'
php ../drupal-installer/composer2.phar require 'drupal/taxonomy_max_depth'

#Modules de formulaires
php ../drupal-installer/composer2.phar require 'drupal/webform'
php ../drupal-installer/composer2.phar require 'drupal/captcha'
php ../drupal-installer/composer2.phar require 'drupal/recaptcha'
php ../drupal-installer/composer2.phar require 'drupal/recaptcha_v3'
php ../drupal-installer/composer2.phar require 'drupal/honeypot'

#Modules de développement
php ../drupal-installer/composer2.phar require 'drupal/devel'
php ../drupal-installer/composer2.phar require 'drupal/yaml_editor'
php ../drupal-installer/composer2.phar require 'drupal/config_actions'
php ../drupal-installer/composer2.phar require 'drupal/rebuild_cache_access'

#Modules SEO
php ../drupal-installer/composer2.phar require 'drupal/metatag'
php ../drupal-installer/composer2.phar require 'drupal/simple_sitemap'
php ../drupal-installer/composer2.phar require 'drupal/easy_breadcrumb'

#Modules Mails
php ../drupal-installer/composer2.phar require 'drupal/mailsystem'
php ../drupal-installer/composer2.phar require 'drupal/symfony_mailer'
php ../drupal-installer/composer2.phar require 'drupal/reroute_email'

#Search
php ../drupal-installer/composer2.phar require 'drupal/search_api'

#Modules contenus
php ../drupal-installer/composer2.phar require 'drupal/image_widget_crop'
php ../drupal-installer/composer2.phar require 'drupal/smart_trim'
php ../drupal-installer/composer2.phar require 'drupal/inline_entity_form'
php ../drupal-installer/composer2.phar require 'drupal/rabbit_hole'
php ../drupal-installer/composer2.phar require 'drupal/content_lock'

#Modules médias
php ../drupal-installer/composer2.phar require 'drupal/entity_browser'
php ../drupal-installer/composer2.phar require 'drupal/entity_embed'
php ../drupal-installer/composer2.phar require 'drupal/imce'

#Modules de sécurité
php ../drupal-installer/composer2.phar require 'drupal/seckit'
php ../drupal-installer/composer2.phar require 'drupal/login_security'
php ../drupal-installer/composer2.phar require 'drupal/password_policy'
php ../drupal-installer/composer2.phar require 'drupal/flood_control'
php ../drupal-installer/composer2.phar require 'drupal/ip_anon'
php ../drupal-installer/composer2.phar require 'drupal/rename_admin_paths'

#Modules de performance
php ../drupal-installer/composer2.phar require 'drupal/advagg'
php ../drupal-installer/composer2.phar require 'drupal/jquery_ui_sortable'

#Thèmes BO
php ../drupal-installer/composer2.phar require 'drupal/adminimal_theme'
php ../drupal-installer/composer2.phar require 'drupal/adminimal_admin_toolbar'

#Librairies
echo "${orange}Création du répertoire de librairies... ${blanc}"
cd web/
mkdir libraries
cd libraries/
wget https://github.com/desandro/imagesloaded/archive/master.zip && mv master.zip imagesloaded-master.zip
wget https://github.com/desandro/masonry/archive/master.zip && mv master.zip masonry-master.zip
unzip imagesloaded-master.zip
unzip masonry-master.zip
rm imagesloaded-master.zip masonry-master.zip
mv imagesloaded-master imagesloaded
mv masonry-master masonry

cd ../../

#Suppression des GIT inutiles
rm -Rf web/.gitignore
mv ../drupal-installer/.gitignore .gitignore

#Déclaration d'un tableau de modules Drupal officiels obligatoires à activer
echo "${orange}Installation des modules obligatoires... ${blanc}"
#Modules d'amélioration du Drupal
vendor/drush/drush/drush en module_filter -y
vendor/drush/drush/drush en admin_toolbar admin_toolbar_tools admin_toolbar_links_access_filter -y
vendor/drush/drush/drush en views_bulk_operations -y
vendor/drush/drush/drush en pathauto -y
vendor/drush/drush/drush en ultimate_cron -y
vendor/drush/drush/drush en role_delegation -y
vendor/drush/drush/drush en simple_menu_permissions -y
vendor/drush/drush/drush en allowed_formats -y
vendor/drush/drush/drush en diff -y
vendor/drush/drush/drush en redirect -y
vendor/drush/drush/drush en menu_item_extras -y
vendor/drush/drush/drush en quick_node_clone -y
vendor/drush/drush/drush en scheduler -y
vendor/drush/drush/drush en scheduler_content_moderation_integration -y
vendor/drush/drush/drush en broken_link -y

#Autres modules
vendor/drush/drush/drush en editor_advanced_link -y
vendor/drush/drush/drush en menu_link_attributes -y
vendor/drush/drush/drush en taxonomy_max_depth -y

#Modules de formulaires
vendor/drush/drush/drush en webform webform_ui -y
vendor/drush/drush/drush en captcha -y
vendor/drush/drush/drush en recaptcha recaptcha_v3 -y
vendor/drush/drush/drush en honeypot -y

#Modules de développement
vendor/drush/drush/drush en devel -y
vendor/drush/drush/drush en yaml_editor -y
vendor/drush/drush/drush en config_actions -y
vendor/drush/drush/drush en rebuild_cache_access -y

#Modules SEO
vendor/drush/drush/drush en metatag metatag_open_graph metatag_twitter_cards metatag_views -y
vendor/drush/drush/drush en simple_sitemap -y
vendor/drush/drush/drush en easy_breadcrumb -y

#Modules Mails
vendor/drush/drush/drush en mailsystem symfony_mailer -y
vendor/drush/drush/drush en reroute_email -y

#Search
vendor/drush/drush/drush en search_api search_api_db -y

#Modules contenus
vendor/drush/drush/drush en image_widget_crop -y
vendor/drush/drush/drush en smart_trim -y
vendor/drush/drush/drush en inline_entity_form -y
vendor/drush/drush/drush en rabbit_hole rh_node rh_taxonomy -y
vendor/drush/drush/drush en content_lock -y

#Modules médias
vendor/drush/drush/drush en entity_browser -y
vendor/drush/drush/drush en entity_embed -y
vendor/drush/drush/drush en imce -y

#Modules de sécurité
vendor/drush/drush/drush en seckit -y
vendor/drush/drush/drush en login_security password_policy -y
vendor/drush/drush/drush en flood_control -y
vendor/drush/drush/drush en ip_anon -y
vendor/drush/drush/drush en rename_admin_paths -y

#Modules de performance
vendor/drush/drush/drush en advagg -y
vendor/drush/drush/drush en jquery_ui_sortable -y

#Modules Core à installer
vendor/drush/drush/drush en workflows content_moderation -y

#Désinstallation
vendor/drush/drush/drush pmu contact -y

#Thèmes BO
vendor/drush/drush/drush then adminimal_theme -y
vendor/drush/drush/drush en adminimal_admin_toolbar -y

echo "${vert}Fin de l'installation des modules obligatoires... ${blanc}"

#Désinstallation de modules
echo "${orange}Désinstallation de modules... ${blanc}"
vendor/drush/drush/drush cron
vendor/drush/drush/drush pmu quickedit search page_cache big_pipe dynamic_page_cache -y
echo "${vert}Fin de la désinstallation de modules... ${blanc}"

#NC Drupal by Net.Com
echo "${orange}Récupération de NC Drupal by Net.Com... ${blanc}"
cd web/modules/ && mkdir custom && cd custom/

wget https://gitlab.com/mmagnyNC/nc-drupal/-/archive/master/nc-drupal-master.zip && unzip nc-drupal-master.zip
rm nc-drupal-master.zip
mv nc-drupal-master/nc_download nc_download
mv nc-drupal-master/nc_links nc_links
mv nc-drupal-master/nc_project nc_project
mv nc-drupal-master/nc_same_tag nc_same_tag
mv nc-drupal-master/nc_share nc_share
mv nc-drupal-master/nc_social nc_social
mv nc-drupal-master/nc_twitter nc_twitter
mv nc-drupal-master/nc_editor nc_editor
mv nc-drupal-master/libraries/* ../../libraries/*

echo "${orange}Installation du thème Net.Com... ${blanc}"
mv nc-drupal-master/project ../../themes/project
rm -Rf nc-drupal-master
cd ../../../
vendor/drush/drush/drush then project
vendor/drush/drush/drush config-set system.theme default project -y
vendor/drush/drush/drush config-set system.theme admin adminimal_theme -y
vendor/drush/drush/drush theme:uninstall bartik

echo "${orange}Installation des modules Net.Com... ${blanc}"

while [ -z ${nc_download_install} ]; do
    echo "Voulez-vous installer le module nc_download ? (y/n) "
    read nc_download_install
done
if [ "$nc_download_install" = "y" ]
then
    vendor/drush/drush/drush en nc_download -y
fi

while [ -z ${nc_links_install} ]; do
    echo "Voulez-vous installer le module nc_links ? (y/n) "
    read nc_links_install
done
if [ "$nc_links_install" = "y" ]
then
    vendor/drush/drush/drush en nc_links -y
fi

while [ -z ${nc_same_tag_install} ]; do
    echo "Voulez-vous installer le module nc_same_tag ? (y/n) "
    read nc_same_tag_install
done
if [ "$nc_same_tag_install" = "y" ]
then
    vendor/drush/drush/drush en nc_same_tag -y
fi

while [ -z ${nc_share_install} ]; do
    echo "Voulez-vous installer le module nc_share ? (y/n) "
    read nc_share_install
done
if [ "$nc_share_install" = "y" ]
then
    vendor/drush/drush/drush en nc_share -y
fi

while [ -z ${nc_social_install} ]; do
    echo "Voulez-vous installer le module nc_social ? (y/n) "
    read nc_social_install
done
if [ "$nc_social_install" = "y" ]
then
    vendor/drush/drush/drush en nc_social -y
fi

while [ -z ${nc_twitter_install} ]; do
    echo "Voulez-vous installer le module nc_twitter ? (y/n) "
    read nc_twitter_install
done
if [ "$nc_twitter_install" = "y" ]
then
    vendor/drush/drush/drush en nc_twitter -y
fi

echo "${vert}Fin de l'installation des modules Net.Com... ${blanc}"

#Création d'un admin projet
echo "${orange}Création du rôle Co-Administrateur... ${blanc}"
vendor/drush/drush/drush role-create "co_admin" "Co-Administrator"

echo "${orange}Compte co-administrateur du client... ${blanc}"
while [ -z ${user_mail} ]; do
    echo "Veuillez saisir l'email de l'admin client (admin_${alias_projet}) du site : "
    read user_mail
done

while [ -z ${user_pw} ]; do
    echo "Veuillez saisir le mot de passe de l'admin client (admin_${alias_projet}) du site : "
    read user_pw
done

echo "${orange}Création du compte Co-Administrateur du client... ${blanc}"
vendor/drush/drush/drush user-create admin_${alias_projet} --mail=${user_mail} --password=${user_pw}
vendor/drush/drush/drush user-add-role "co_admin" admin_${alias_projet}

vendor/drush/drush/drush en nc_project -y
vendor/drush/drush/drush en nc_editor -y
echo "${vert}Fin de la création du compte Co-Administrateur du client... ${blanc}"

#Configurer le mode développement
cd web/sites/
mv ../../../drupal-installer/development.services.yml development.services.yml
mv ../../../drupal-installer/settings.local.php default/settings.local.php

cd default/
chmod 666 settings.php
echo " " >> settings.php
echo "if (file_exists(\$app_root . '/' . \$site_path . '/settings.local.php')) {" >> settings.php
echo "  include \$app_root . '/' . \$site_path . '/settings.local.php';" >> settings.php
echo "}" >> settings.php
echo "\$settings['file_temp_path'] = '/tmp';" >> settings.php
echo "\$settings['file_private_path'] = '';" >> settings.php
chmod 444 settings.php
cd ../../../

#Fichier de déploiement
mv ../drupal-installer/.gitlab-ci.yml .gitlab-ci.yml

#Rebuild cache
echo "${orange}Reconstruction du cache du site... ${blanc}"
vendor/drush/drush/drush cr

echo "${vert}Fin de l'installation... ${blanc}"

echo "${vert}Félicitation !!!${blanc}"
